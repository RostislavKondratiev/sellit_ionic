import { Author } from './author.model';

export class Product {
    public id: number;
    public title: string;
    public description: string;
    public author: Author;
    public price: string;
    public 'photo_details': any;
    public 'date_create': string;
    public 'date_update': string;
    constructor(data) {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.author = new Author(data.author_details);
        this.price = data.price;
        this.photo_details = data.photo_details;
        this.date_create = data.date_create;
        this.date_update = data.date_update;
    }
}
