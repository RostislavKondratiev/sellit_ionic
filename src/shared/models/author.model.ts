import { ApiConfig } from '../configs/apiConfig';

export class Author {
    public id: number;
    public firstName: string;
    public lastName: string;
    public username: string;
    public email: string;
    public photo: any;

    constructor(data) {
        this.id = data.id;
        this.firstName = data.first_name;
        this.lastName = data.last_name;
        this.username = data.username;
        this.email = data.email;
        this.photo = data.photo.photo ? `${ApiConfig.base}${data.photo.photo}` : null;
    }

}
