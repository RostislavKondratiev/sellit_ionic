export class ChangePass {
    public 'new_password1': string;
    public 'new_password2': string;
    public 'old_password': string;

    constructor(data) {
        this.old_password = data.old;
        this.new_password1 = data.password;
        this.new_password2 = data.confirm;
    }
}
