export class ApiConfig {
  public static base = 'http://fe-kurs.light-it.loc:80';
  public static api = `${ApiConfig.base}/api`;
}
