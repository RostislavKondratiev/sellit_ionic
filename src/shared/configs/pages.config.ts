import { ProductsPage } from '../../pages/products/products';
import { CreateProductPage } from '../../pages/create-product/create-product';
import { ProfilePage } from '../../pages/profile/profile';

export class PagesConfig{
  public static authorized = [
    { title: 'Products', component: ProductsPage},
    { title: 'Add Product', component: CreateProductPage},
    { title: 'User Profile', component: ProfilePage}
  ];
  public static nonAuthorized = [
    { title: 'Products', component: ProductsPage}
  ]
}
