import { Directive, Output, EventEmitter, HostListener } from '@angular/core';
@Directive({
    selector: '[photo-preview]'
})
export class PhotoPreviewDirective {
    @Output() public photoEvent: EventEmitter<any> = new EventEmitter<any>();
    private typesList: string = 'jpg png';
    /**
     * Emitting photo base64 value to component
     * @param event change event
     */
    @HostListener('change', ['$event'])
    public photoPreview(event) {
        if (event.target.files && event.target.files.length > 0) {
            for (let photo of event.target.files) {
                let tmp = photo.name.split('.').slice(-1)[0];
                if (this.typesList.match(tmp)) {
                    let reader = new FileReader();
                    reader.onload = (e: any) => {
                        this.photoEvent.emit(e.target.result);
                    };
                    reader.readAsDataURL(photo);
                }
            }
        }
    }
}
