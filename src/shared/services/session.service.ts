import { Injectable } from '@angular/core';
import { Author } from '../models/author.model';
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SessionService {
  private userState: Subject<any>;
  constructor() {
    this.userState = new BehaviorSubject<any>(this.userData);
  }
  public set userData(data: any){
    localStorage.setItem('user', JSON.stringify(new Author(data)));
    if(data.token){
      localStorage.setItem('token', JSON.stringify(data.token));
    }
    this.userState.next(new Author(data));
  }
  public isLogin() {
    return this.userData && this.token;
  }
  public get userData(){
    return JSON.parse(localStorage.getItem('user'));
  }
  public get token(){
    return JSON.parse(localStorage.getItem('token'));
  }
  public logout(){
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.userState.next(null);
  }
  public get userStateListener() {
    return this.userState.asObservable();
  }
}
