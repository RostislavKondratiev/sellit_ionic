import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class NetworkService {
  constructor(private network: Network){}
  public watchConnection(){
    return this.network.onDisconnect();
  }
}
