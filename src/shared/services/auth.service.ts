import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiConfig } from '../configs/apiConfig';
import { LoginModel } from '../models/login.model';
import { RegistrationModel } from '../models/registration.model';
import { SessionService } from './session.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  constructor(private http: Http, private session: SessionService) {}
  public login(data){
    return this.http.post(`${ApiConfig.api}/login/`, new LoginModel(data)).map((res) => {
      console.log(res);
      this.session.userData = res.json();
      return res.json();
    }).catch((err) => {
      return Observable.throw(this.loginErrorHandler(err.json()));
    })
  }
  public register(data){
    return this.http.post(`${ApiConfig.api}/signup/`, new RegistrationModel(data)).map((res) => res.json());
  }
  public logout(){
    this.session.logout();
  }
  private loginErrorHandler(error){
    let list = [];
    for (let item in error) {
      if (error.hasOwnProperty(item)) {
        list.push(`${item}: ${error[item][0]}`);
      }
    }
    return list;
  }
}
