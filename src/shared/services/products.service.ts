import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { ApiConfig } from '../configs/apiConfig';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';

import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';

@Injectable()
export class ProductsService {
  private limit: string = '12';
  private cachedProducts: Product[] = [];
  constructor(private http: Http, private storage: Storage, private network: Network) {}
  public getProducts(offset: string | number = 0, title?: string): Observable<Product[]> {
    this.limit = '12';
    if(this.network.type === 'none') {
      return Observable.fromPromise(this.storage.get('products')).map((res) => {
        let data = res;
        console.log(data);
        if(title){
          console.log('search');
          data = res.filter((item) => item.title.match(title));
          console.log(data);
        }
        if(offset !== 0) {
          return data.splice(offset, +this.limit);
        } else {
          return data.splice(0, +this.limit);
        }
      });
    } else {
      let params: URLSearchParams = new URLSearchParams();
      if (title) {
        params.set('search', title);
      }
      params.set('limit', this.limit);
      params.set('offset', offset.toString());
      console.log(this.limit, offset);
      if(offset.toString() === '0'){
        this.cachedProducts = [];
      }
      return this.http.get(`${ApiConfig.api}/poster/`,{ search: params }).map((res) => {
        let tmp = [];
        let data: any = res.json().results;
        data.forEach((item) => {
          tmp.push(new Product(item));
        });
        this.cachedProducts.push(...tmp);
        console.log(this.cachedProducts);
        this.storage.set('products', this.cachedProducts);
        return tmp;
      });
    }
  }
  public addProduct(form, photos): Observable<Product> {
    console.log(1111);
    let addForm = form as FormGroup;
    let value = new FormData();
    let photoList = [];
    for (let item of photos) {
      photoList.push(item);
      value.append('photo', item);
    }
    // value.append('photo', photo);
    console.log(photos);
    return this.newPhoto(value)
      .mergeMap((res: number[]) => {
        console.log(res);
        return this.newProduct(res, addForm);
      });
  }
  public updateProduct(id: number, form: FormGroup, photos: any, oldPhotos: number[]): Observable<Product> {
    let updateForm = form as FormGroup;
    if (photos.length > 0) {
      console.log('photos+')
      let value = new FormData();
      let photolist = [];
      for (let item of photos) {
        photolist.push(item);
        value.append('photo', item);
      }
      oldPhotos = oldPhotos.length > 0 ? oldPhotos : [];
      console.log(oldPhotos);
      return this.addPhoto(value, oldPhotos)
        .flatMap((res) => {
          console.log(res);
          return this.updatePost(res, updateForm, id);
        });
    } else {
      return this.updatePost(oldPhotos, updateForm, id);
    }
  }
  public deleteProduct(id): Observable<any> {
    return this.http.delete(`${ApiConfig.api}/poster/${id}/`).map((res) => res.json());
  }

  private newPhoto(value): Observable<number[]> {
    return this.http.post(`${ApiConfig.api}/photo/`, value)
      .map((res) => {
        let photoListId = [];
        for (let item of res.json()) {
          photoListId.push(item.id);
        }
        console.log(photoListId);
        return photoListId;
      });
  }

  private newProduct(value, addForm): Observable<Product> {
    addForm.patchValue({
      photos: value,
      date_create: new Date()
    });
    console.log(addForm.value);
    return this.http.post(`${ApiConfig.api}/poster/`, addForm.value).map((val) => val.json());
  }

  private addPhoto(value: FormData, oldPhotos: number[]): Observable<number[]> {
    return this.http.post(`${ApiConfig.api}/photo/`, value)
      .map((res) => {
        for (let item of res.json()) {
          oldPhotos.push(item.id);
        }
        return oldPhotos;
      });
  }
  private updatePost(value: number[], form: FormGroup, id: number): Observable<Product> {
    console.log(value);
    form.patchValue({
      photos: value
    });
    return this.http.put(`${ApiConfig.api}/poster/${id}/`, form.value)
      .map((res) => new Product(res.json()));
  }
}
