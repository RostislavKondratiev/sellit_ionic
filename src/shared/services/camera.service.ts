import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class CameraService {
  constructor(private camera: Camera) {}
  public takePhoto(source) {
    let sourceType;
    let photo;
    let photoObj;
    if(source === 'camera') {
      sourceType = this.camera.PictureSourceType.CAMERA;
    }
    else if(source === 'photolibrary'){
      sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: sourceType,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false,
      correctOrientation: true,
    };
    return this.camera.getPicture(options).then((image) => {
      let base64 = 'data:image/jpeg;base64,' + image;
      photo = base64;
      photoObj = this.dataURLtoFile(base64, 'userphoto.jpeg');
      return {
        photoUrl: photo,
        photoObj: photoObj
      }
    }, (err) => {
      console.log(err);
    })
  }
  private dataURLtoFile(dataurl, filename) {
    let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
  }
}
