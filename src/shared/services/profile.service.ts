import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiConfig } from '../configs/apiConfig';
import { ChangePass } from '../models/changePass.model';
import { SessionService } from './session.service';
import { Author } from '../models/author.model';
import { Observable } from 'rxjs/Observable';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Injectable()
export class ProfileService {
  constructor(private http: Http, private session: SessionService, private notification: LocalNotifications) {}
  public getProfile(): Observable<Author> {
    return this.http.get(`${ApiConfig.api}/profile/me/`).map((res) => res.json());
  }
  public updateProfile(): void {
    this.getProfile().subscribe((res: Author) => {
      this.session.userData = res;
    });
  }
  public changePass(data){
      return this.http.post(`${ApiConfig.api}/change_password/`, new ChangePass(data)).map((res) => {
        this.notification.schedule({
          id: 1,
          title: 'Sell IT',
          text: 'Password Changes',
          icon: 'http://example.com/icon.png'
        });
        return res.json();
      });
  }
  public changePhoto(data: any){
    let input = new FormData();
    input.append('photo', data);
    return this.http.post(`${ApiConfig.api}/photo/`, input)
      .map((res) => {
        let value = res.json();
        return value[0];
      })
      .mergeMap((res) => this.http.post(`${ApiConfig.api}/profile_photo/`,
        {user: this.session.userData.id, photo: res.id}));
  }
}
