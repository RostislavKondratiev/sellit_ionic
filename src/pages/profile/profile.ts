import { Component, ViewChild } from '@angular/core';
import { SessionService } from '../../shared/services/session.service';
import { Author } from '../../shared/models/author.model';
import { ModalController, NavController } from 'ionic-angular';
import { ChangePassword } from './change-password/change-password';
import { ChangePhoto } from './change-photo/change-photo';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs/Subscription';
import { Sim } from '@ionic-native/sim';


@Component({
  selector: 'profile',
  templateUrl: './profile.html'
})
export class ProfilePage{
  @ViewChild('map') mapElement;
  public map: any;
  public user: Author;
  public tmp = null;
  public latLng;
  public serial: string = '';
  private subs: Subscription[] = [];

  constructor(
    private session: SessionService,
    public modalCtrl: ModalController,
    public nav: NavController,
    private geolocation: Geolocation,
    private sim: Sim
  ) {
  }

  public ionViewWillEnter() {
    this.user = this.session.userData;
    this.sim.requestReadPermission().then(
      () => console.log('Permission granted'),
      () => console.log('Permission denied')
    );
    this.sim.getSimInfo().then((res) => {
      this.serial = res.simSerialNumber;
    });
    this.subs.push(this.geolocation.watchPosition().subscribe((position) => {
      this.latLng = {lat: position.coords.latitude, lng: position.coords.longitude};
    }));
  }

  public passwordModal() {
    let modal = this.modalCtrl.create(ChangePassword);
    modal.present();
  }

  public photoModal() {
    let modal = this.modalCtrl.create(ChangePhoto);
    modal.present();
  }


  public ionViewDidLoad() {
    this.loadMap();
  }

  public loadMap(){
    this.geolocation.getCurrentPosition().then((position) => {
      this.latLng = {lat: position.coords.latitude, lng: position.coords.longitude};
    }, (err) => {
      console.log(err);
    });

  }
  public ionViewDidLeave(){
    this.subs.forEach((s) => {
      s.unsubscribe();
    })
  }
}
