import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '../../../shared/services/profile.service';
import { App, NavController } from 'ionic-angular';
import { AuthService } from '../../../shared/services/auth.service';
import { NavigationPage } from '../../navigation/navigation';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.html'
})
export class ChangePassword {
  public changeForm: FormGroup;
  constructor(public nav: NavController, private profileService: ProfileService, private authService: AuthService, private app: App){
    this.changeForm = new FormGroup({
      old: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      confirm: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }
  public submit($event) {
    $event.preventDefault();
    this.profileService.changePass(this.changeForm.value).subscribe((res) => {
      this.authService.logout();
      this.nav.pop().then((res) => {
        this.app.getActiveNav().setRoot(NavigationPage);
      });
    });
  }
  public closeModal(){
    this.nav.pop();
  }
}
