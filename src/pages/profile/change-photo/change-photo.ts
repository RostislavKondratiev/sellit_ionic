import { Component } from '@angular/core';
import { SessionService } from '../../../shared/services/session.service';
import { ProfileService } from '../../../shared/services/profile.service';
import { App, NavController } from 'ionic-angular';
import { CameraService } from '../../../shared/services/camera.service';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { NavigationPage } from '../../navigation/navigation';


@Component({
  selector: 'change-photo',
  templateUrl: './change-photo.html'
})
export class ChangePhoto {
  public photo;
  public serverError;
  public tmpPhoto = null;
  constructor(public nav: NavController,
              private session: SessionService,
              private profileService: ProfileService,
              private app: App,
              private cameraService: CameraService,
              private localNotification: PhonegapLocalNotification
  ) {
    this.photo = this.session.userData.photo;
  }
  public fromCamera(){
    this.cameraService.takePhoto('camera').then((image: any) => {
      if(image) {
        this.photo = image.photoUrl;
        this.tmpPhoto = image.photoObj;
      }
    });
  }
  public fromLibrary(){
    this.cameraService.takePhoto('photolibrary').then((image: any) => {
      if(image) {
        this.photo = image.photoUrl;
        this.tmpPhoto = image.photoObj;
      }
    });
  }
  public closeModal(){
    this.nav.pop();
  }
  public addPhoto($event) {
    $event.preventDefault();
    if(this.tmpPhoto) {
      let data = this.tmpPhoto;
      this.profileService.changePhoto(data).subscribe((res) => {
        this.profileService.updateProfile();
        this.localNotification.requestPermission().then(
          (permission) => {
            if (permission === 'granted') {

              // Create the notification
              this.localNotification.create('Sell IT', {
                tag: 'message1',
                body: 'Photo Changed',
              });

            }
          }
        );
        this.nav.pop().then((res) => {
          this.app.getActiveNav().setRoot(NavigationPage);
        });
      }, (err) => {
        this.serverError = err.json();
      });
    }
  }
}
