import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductDetailsPage } from '../../product-details/product-details';

@Component({
  selector: 'products-item',
  templateUrl: './products-item.html'
})
export class ProductsItem {
  @Input() public product;

  constructor(public nav: NavController) {}
  public goToDetails(){
    console.log(this.product);
    this.nav.push(ProductDetailsPage, {product: this.product})
  }
}
