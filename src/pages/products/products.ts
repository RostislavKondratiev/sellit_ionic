import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductsService } from '../../shared/services/products.service';
import { Product } from '../../shared/models/product.model';
import { LoadingController } from 'ionic-angular';
// import { Network } from '@ionic-native/network';
// import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'products',
  templateUrl: './products.html'
})
export class ProductsPage {
  public products = [];
  public isEmpty = false;
  public loader = this.loading.create({
    content: "Please wait...",
  });
  public connection: string;
  private isEnough = false;
  private offset: number = 0;
  private search: string;
  // private subs: Subscription[] = [];
  constructor(public nav: NavController,
              private productsService: ProductsService,
              private loading: LoadingController,
  ) {
  }
  public ionViewWillEnter() {
    this.loader.present();
    this.productsService.getProducts().subscribe((res) => {
      setTimeout(() => {
        this.products = res;
        console.log(res);
        this.loader.dismiss();
      }, 2000);
    }, (err) => {
      this.loader.dismiss();
    });
  }
  public onInput($event) {
    this.search = $event.target.value;
    if(this.search.length === 0) {
      console.log(this.search);
      this.productsService.getProducts().subscribe((res) => {
        this.products = res;
      });
    } else {
      this.searchResult(this.search);
    }
  }
  public infinite($event) {
      console.log(this.isEnough);
      if (!this.isEnough) {
        this.offset += 12;
        let prevDataLength = this.products.length;
        console.log(this.search);
        this.productsService.getProducts(this.offset, this.search).subscribe((res: Product[]) => {
          this.products.push(...res);
          $event.complete();
          if (prevDataLength === this.products.length) {
            this.isEnough = true;
            $event.complete();
          }
        });
      } else {
        $event.complete();
      }
    }
  public doRefresh(refresher){
    this.offset = 0;
    this.search = '';
    this.productsService.getProducts().subscribe((res) => {
      this.products = res;
      refresher.complete();
    })
  }
  // private getData(){
  //   if(this.network.type === 'none') {
  //     this.storage.get('products').then((res) => {
  //       console.log(res);
  //       this.products = res;
  //       this.loader.dismiss();
  //     })
  //   } else {
  //     this.productsService.getProducts().subscribe((res) => {
  //       setTimeout(() => {
  //         this.storage.set('products', res);
  //         this.products = res;
  //         this.loader.dismiss();
  //       }, 2000);
  //     })
  //   }
  // }
  private searchResult(search?: string) {
    if (search) {
      console.log('search');
      this.productsService.getProducts(0, search)
        .subscribe((res: Product[]) => {
          this.products = [];
          this.products.push(...res);
          if ( this.products.length === 0) {
            this.isEmpty = true;
          } else {
            this.isEmpty = false;
          }
        });
    }
  }
}
