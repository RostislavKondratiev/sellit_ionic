import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionService } from '../../shared/services/session.service';
import { NavController, NavParams } from 'ionic-angular';
import { Product } from '../../shared/models/product.model';
import { ProductsService } from '../../shared/services/products.service';
import { ProductsPage } from '../products/products';
import { ProductDetailsPage } from '../product-details/product-details';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.html'
})
export class EditProductPage implements OnInit {
  public editForm: FormGroup;
  public product: Product;
  public photoUrls = [];
  public oldPhotos = [];
  public photos = [];
  public editorOptions = {
    heightMin: 100,
    heightMax: 200,
  };

  constructor(public nav: NavController, public navParams: NavParams, private session: SessionService, private productsService: ProductsService) {
    this.product = this.navParams.get('product');
    this.editForm = new FormGroup({
      title: new FormControl(this.product.title || null, [Validators.required]),
      price: new FormControl(this.product.price || null, [Validators.required]),
      author: new FormControl(this.session.userData.id),
      description: new FormControl(this.product.description || null),
      photos: new FormControl(),
      date_create: new FormControl(this.product.date_create)
    });
  }

  public ngOnInit() {
    for (let item of this.product.photo_details) {
      this.oldPhotos.push(item.photo);
      this.photos.push(item.id);
      this.photoUrls.push(item.photo);
    }
  }

  public submitForm(event: any, photo: any): void {
    event.preventDefault();
    if (this.editForm.valid) {
      if ((photo.files && photo.files.length > 0) || this.oldPhotos.length > 0) {
        console.log(this.oldPhotos);
        this.productsService.updateProduct(this.product.id, this.editForm, photo.files, this.photos).subscribe((res) => {
          this.nav.setRoot(ProductsPage);
          this.nav.push(ProductDetailsPage, {product: res});
        });
      }
    }
  }
  public deletePhoto(id) {
    if ( id < this.oldPhotos.length) {
      this.oldPhotos.splice(id, 1);
      this.photos.splice(id, 1);
      this.photoUrls.splice(id, 1);
    }
  }
}

