import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { ModalController, NavController } from 'ionic-angular';
import { LoginModal } from '../login/login';

@Component({
  selector: 'registration',
  templateUrl: './registration.html'
})
export class RegistrationModal {
  public registrationForm: FormGroup;
  constructor(public nav: NavController, private authService: AuthService, public modalCtrl: ModalController) {
    this.registrationForm = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      confirm: new FormControl(null, [Validators.required])
    })
  }
  public submit($event) {
    $event.preventDefault();
    this.authService.register(this.registrationForm.value).subscribe((res) => {
      this.nav.pop();
      let modal = this.modalCtrl.create(LoginModal);
      modal.present();
    })
  }
  public closeModal(){
    this.nav.pop();
  }
}
