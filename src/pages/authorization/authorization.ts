import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { LoginModal } from './login/login';
import { ProductsPage } from '../products/products';
import { RegistrationModal } from './registration/registration';

@Component({
  selector: 'authorization',
  templateUrl: './authorization.html'
})
export class AuthorizationPage {
  constructor(public nav: NavController, public modalCtrl: ModalController) {}

  public loginModal(){
    let modal = this.modalCtrl.create(LoginModal);
    modal.present();
  }
  public registrationModal() {
    let modal = this.modalCtrl.create(RegistrationModal);
    modal.present();
  }
  public rootHandler(){
    this.nav.setRoot(ProductsPage);
  }
}
