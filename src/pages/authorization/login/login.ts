import { Component } from '@angular/core';
import { AlertController, App, NavController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { NavigationPage } from '../../navigation/navigation';

@Component({
  selector: 'login',
  templateUrl: './login.html'
})
export class LoginModal {
  public loginForm: FormGroup;
  constructor(public nav: NavController,public alertCtrl: AlertController, private authService: AuthService, private app: App) {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    })
  }
  public closeModal(){
    this.nav.pop();
  }
  public submit($event) {
    $event.preventDefault();
    this.authService.login(this.loginForm.value).subscribe((res) => {
      this.nav.pop().then((res) => {
        this.app.getActiveNav().setRoot(NavigationPage);
      });
    }, (err) => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: `${err}`,
        buttons: [
          {
            text: 'Ok',
            role: 'cancel'
          }
        ]
      });
      alert.present();
    });
  }
}
