import { Component, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Author } from '../../shared/models/author.model';
import { AuthService } from '../../shared/services/auth.service';
import { SessionService } from '../../shared/services/session.service';
import { PagesConfig } from '../../shared/configs/pages.config';
import { AuthorizationPage } from '../authorization/authorization';
import { Nav } from 'ionic-angular';
import { ProductsPage } from '../products/products';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Vibration } from '@ionic-native/vibration';


@Component({
  selector: 'navigation',
  templateUrl: './navigation.html'
})
export class NavigationPage {
  @ViewChild(Nav) nav: Nav;
  public rootPage = ProductsPage;
  public subs: Subscription[] = [];
  public user: Author = null;
  public connection: string;
  private noInternetToast;
  private presented: boolean = false;
  pages: Array<{title: string, component: any}>;

  constructor(
    private authService: AuthService,
    private session: SessionService,
    private localNotifications: LocalNotifications,
    private network: Network,
    private toast: ToastController,
    private vibration: Vibration,
  ) {}
  public ionViewWillEnter(){
    this.subs.push(this.session.userStateListener.subscribe((val) => {
      this.user = val;
      this.connection = this.network.type;
      if(this.user){
        this.pages = PagesConfig.authorized;
      } else {
        this.pages = PagesConfig.nonAuthorized
      }
    }));
    this.subs.push(this.network.onchange().subscribe(() => {
      this.connection = this.network.type;
      if(this.network.type === 'none'){
        this.noInternetToast = this.toast.create({message: 'No internet, functionality shortcuted', showCloseButton: true});
        this.noInternetToast.present();
        this.vibration.vibrate(1000);
        this.presented = true;
      } else {
        if(this.presented) {
          this.noInternetToast.dismiss();
        }
      }
    }));
    this.subs.push(this.network.onDisconnect().subscribe(() => {
      if(this.network.type !== 'none'){
        this.toast.create({message: `Lost Connection - ${this.connection}`, duration: 3000}).present();
      }
    }));
    this.subs.push(this.network.onConnect().subscribe(() => {
      // if (this.connection !== this.network.type && this.network.type !== 'none') {
      setTimeout(() => {
        this.toast.create({message: `You are connected across - ${this.network.type}`, duration: 3000}).present();
      },3000)
      // }
    }));
    let today = new Date();
    today.setHours(9);
    today.setMinutes(0);
    today.setSeconds(0);
    let todayAt9 = new Date(today);
    this.localNotifications.schedule({
      id: 3,
      text: 'Checkout Our App',
      firstAt: todayAt9,
      every: 'day',
      led: 'FF0000',
      sound: null
    });
  }
  public openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  public logout(){
    this.authService.logout();
  }
  public authorize(){
    this.nav.setRoot(AuthorizationPage);
  }
  public ionViewDidLeave(){
    this.subs.forEach((s) => {
      s.unsubscribe();
    })
  }
}
