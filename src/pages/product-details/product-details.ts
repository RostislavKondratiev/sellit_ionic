import { Component } from '@angular/core';
import { ActionSheetController, NavController, NavParams } from 'ionic-angular';
import { Product } from '../../shared/models/product.model';
import { ProductsService } from '../../shared/services/products.service';
import { ProductsPage } from '../products/products';
import { Author } from '../../shared/models/author.model';
import { SessionService } from '../../shared/services/session.service';
import { EditProductPage } from '../edit-product/edit-product';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.html'
})
export class ProductDetailsPage{
  public product: Product;
  public user: Author;
  constructor(public nav: NavController,
              private navParams: NavParams,
              private actionSheet: ActionSheetController,
              private productsService: ProductsService,
              private session: SessionService) {}

  public ionViewWillEnter() {
    this.product = this.navParams.get('product');
    this.user = this.session.userData;
    console.log(this.user);

  }
  public openActionSheet(){
    console.log(123);
    let detailsActionSheet = this.actionSheet.create({
      title: 'Control Your Product',
      buttons: [
        {
          text: 'Edit',
          handler: () => {
            this.nav.push(EditProductPage, {product: this.product})
          }
        },{
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            this.productsService.deleteProduct(this.product.id).subscribe((res) => {
              this.nav.setRoot(ProductsPage);
            })
          }
        },
        {
          text: 'Cancel',
          handler: () => {

          }
        }
      ]
    });
    detailsActionSheet.present();
  }
}
