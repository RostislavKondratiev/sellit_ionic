import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionService } from '../../shared/services/session.service';
import { ProductsService } from '../../shared/services/products.service';
import { AlertController, NavController } from 'ionic-angular';
import { ProductsPage } from '../products/products';
import { ProductDetailsPage } from '../product-details/product-details';
import { CameraService } from '../../shared/services/camera.service';

@Component({
  selector: 'create-product',
  templateUrl: './create-product.html'
})
export class CreateProductPage {
  public addProductForm: FormGroup;
  public photos = [];
  public tmpPhoto = [];
  public editorOptions = {
    heightMin: 100,
    heightMax: 200,
  };
  constructor(
    private session: SessionService,
    private productsService: ProductsService,
    public nav: NavController,
    private cameraService: CameraService,
    public alertCtrl: AlertController
  ) {
  this.addProductForm = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      price: new FormControl(null, [Validators.required]),
      author: new FormControl(this.session.userData.id),
      description: new FormControl(null),
      photos: new FormControl(null),
      date_create: new FormControl(null)
    });
  }
  public create($event){
    $event.preventDefault();
    this.productsService.addProduct(this.addProductForm, this.tmpPhoto).subscribe((res) => {
      this.nav.setRoot(ProductsPage);
      this.nav.push(ProductDetailsPage, {product: res})
    })
  }
  public fromLibrary() {
    this.cameraService.takePhoto('photolibrary').then((image: any) => {
      if(image){
        this.photos.push(image.photoUrl);
        this.tmpPhoto.push(image.photoObj);
      }
    });
  }
  public fromCamera(){
    this.cameraService.takePhoto('camera').then((image: any) => {
      if(image){
        this.photos.push(image.photoUrl);
        this.tmpPhoto.push(image.photoObj);
      }
    });
  }
  ionViewCanLeave(){
    return new Promise((resolve, reject) => {
      let confirm = this.alertCtrl.create({
        title: 'Are you sure?',
        message: 'You realy wanna leave?',
        buttons: [{
          text: 'OK',
          handler: () => {
            resolve();
          },
        }, {
          text: 'Cancel',
          handler: () => {
            reject();
          }
        }],
      });
      confirm.present();
    })
  }
}
