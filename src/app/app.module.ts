import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { NavigationPage } from '../pages/navigation/navigation';
import { ListPage } from '../pages/list/list';

import "froala-editor/js/froala_editor.pkgd.min.js";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProductsService } from '../shared/services/products.service';
import { ProductsPage } from '../pages/products/products';
import { ProductsItem } from '../pages/products/products-item/products-item';
import { Http, HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
import { LoginModal } from '../pages/authorization/login/login';
import { RegistrationModal } from '../pages/authorization/registration/registration';
import { AuthorizationPage } from '../pages/authorization/authorization';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { IonicStorageModule } from '@ionic/storage';
import { SessionService } from '../shared/services/session.service';
import { CreateProductPage } from '../pages/create-product/create-product';
import { MyHttp } from '../shared/services/myhttp';
import { EditProductPage } from '../pages/edit-product/edit-product';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileService } from '../shared/services/profile.service';
import { ChangePassword } from '../pages/profile/change-password/change-password';
import { PhotoPreviewDirective } from '../shared/directives/photo-preview.directive';
import { ChangePhoto } from '../pages/profile/change-photo/change-photo';
import { Camera } from '@ionic-native/camera';
import { CameraService } from '../shared/services/camera.service';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Network } from '@ionic-native/network';
import { NetworkService } from '../shared/services/network.service';
import { AgmCoreModule } from '@agm/core';
import { Sim } from '@ionic-native/sim';
import { Vibration } from '@ionic-native/vibration';

@NgModule({
  declarations: [
    MyApp,
    NavigationPage,
    HomePage,
    ListPage,
    ProductsPage,
    ProductsItem,
    ProductDetailsPage,
    LoginModal,
    RegistrationModal,
    AuthorizationPage,
    CreateProductPage,
    EditProductPage,
    ProfilePage,
    ChangePassword,
    ChangePhoto,
    PhotoPreviewDirective,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9iXMcKllOLCX5yxg0s6OmOQ6xHuU65DI'
    }),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NavigationPage,
    HomePage,
    ListPage,
    ProductsPage,
    ProductDetailsPage,
    LoginModal,
    RegistrationModal,
    AuthorizationPage,
    CreateProductPage,
    EditProductPage,
    ProfilePage,
    ChangePassword,
    ChangePhoto,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ProductsService,
    AuthService,
    SessionService,
    ProfileService,
    Camera,
    CameraService,
    Geolocation,
    GoogleMaps,
    LocalNotifications,
    PhonegapLocalNotification,
    BackgroundMode,
    Network,
    NetworkService,
    Sim,
    Vibration,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: Http,
      useClass: MyHttp,
      deps: [XHRBackend, RequestOptions, SessionService]
    }
  ]
})
export class AppModule {}
